package com.example.loginapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.commit
import com.example.loginapp.R
import com.example.loginapp.databinding.FragmentFirstBinding

class FirstFragment : Fragment(R.layout.fragment_first) {

    //private lateinit var bnGoSecondFragment: Button
    private var _binding:FragmentFirstBinding? = null
    private val binding get() = _binding !!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFirstBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.firstFragmentBn.setOnClickListener {
            requireActivity().supportFragmentManager.commit {
                replace(R.id.fragment_activity_fv_container,SecondFragment())
            }
        }

        /*bnGoSecondFragment = view.findViewById(R.id.first_fragment_bn)
        bnGoSecondFragment.setOnClickListener {
            val transaction: FragmentTransaction =
                requireActivity().supportFragmentManager.beginTransaction()
            transaction.setReorderingAllowed(true)
            transaction.replace(
                R.id.fragment_activity_fv_container,
                SecondFragment.newInstance("Alexis", "Lopez"),
                null
            )
            transaction.commit()
        }*/
    }
}