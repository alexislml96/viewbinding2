package com.example.loginapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import com.example.loginapp.R

class SecondFragment : Fragment(R.layout.fragment_second) {

    private var firstName:String? = ""
    private var lastName:String? = ""

    private lateinit var  tvFullName:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        firstName = requireArguments().getString(FIRST_NAME)
        lastName = requireArguments().getString(LAST_NAME)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvFullName = view.findViewById(R.id.second_fragment_tv_fullname)

        tvFullName.text = "$firstName $lastName"
    }

    companion object{
        private const val FIRST_NAME = "FIRST_NAME"
        private const val LAST_NAME = "LAST_NAME"

        fun newInstance(firstName:String,lastName:String) = SecondFragment().apply {
            arguments = bundleOf(FIRST_NAME to firstName, LAST_NAME to lastName)
        }
    }


}